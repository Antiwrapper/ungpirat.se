import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Hero from "../components/hero"

const BoardPage = () => (
    <Layout>
        <SEO title="Om oss" />
        <Hero heroTitle="Om oss" />
        <div id="aboutContent" className="container">
            <div className="content">
                <h1 className="title">Förbundsstyrelsen</h1>
                <p>
                    Vi är Förbundsstyrelsen, det är vi som ansvarar för förbundets centrala verksamhet. Förbundsstyrelsen utses av kongressen och består av Förbundsordförande, Förbundssekreterare och 3 till 9 ledamöter.
                </p>
            </div>
            <div className="tile is-ancestor">
                <div className="tile is-6 is-vertical is-parent">
                    <div className="tile is-child box">
                        <p class="title">Morgan Landström</p>
                        <p>Förbundsordförande</p>
                        <a href="mailto:ordforande@ungpirat.se">ordforande@ungpirat.se</a>
                    </div>
                    <div className="tile is-child box">
                        <p class="title">Magnus Hulterström</p>
                        <p>Förbundssekreterare</p>
                        <a href="mailto:sekreterare@ungpirat.se">sekreterare@ungpirat.se</a>
                    </div>
                </div>
                <div className="tile is-parent">
                    <div className="tile is-child box">
                        <p class="title">Presidiet</p>
                        <p>Presidiet består av Förbundsordförande och Förbundssekreterare.</p>
                    </div>
                </div>
            </div>
            <div className="tile is-ancestor">
                <div className="tile is-parent">
                    <div className="tile is-child box">
                        <p class="title">David Klarlund</p>
                    </div>
                </div>
                <div className="tile is-parent">
                    <div className="tile is-child box">
                        <p class="title">Jakob Sinclair</p>
                    </div>
                </div>
                <div className="tile is-parent">
                    <div className="tile is-child box">
                        <p class="title">Josefine Widberg</p>
                    </div>
                </div>
            </div>
            <div className="tile is-ancestor">
                <div className="tile is-parent">
                    <div className="tile is-child box">
                        <p class="title">Siula Grande</p>
                    </div>
                </div>
                <div className="tile is-parent">
                    <div className="tile is-child box">
                        <p class="title">Kevin Nilsson</p>
                    </div>
                </div>
                <div className="tile is-parent">
                    <div className="tile is-child box">
                        <p class="title">Martin Sjöberg</p>
                    </div>
                </div>
            </div>
        </div>
    </Layout>
)

export default BoardPage
