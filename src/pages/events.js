import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Hero from "../components/hero"

const EventsPage = () => (
    <Layout>
        <SEO title="Evenemang" />
        <Hero heroTitle="Evenemang" />
        <div id="aboutContent" className="container">
            <div className="content">
                <h1 className="title">Evenemang</h1>
                <p>
                    Vi är Ung Pirat, Piratpartiets ungdomsförbund.
                </p>
                <p>
                    Vi står bakom vårt <a href="https://piratpartiet.se/principprogram">modersparti principprogram</a> och <a href="https://piratpartiet.se/sakpolitik">sakpolitik</a> och vi jobbar för:
                    <ul>
                        <li>Mänskliga rättigheter, individens egenmakt och delaktighet.</li>
                        <li>Fri tillgång till kunskap. Rätt att dela kultur.</li>
                        <li>Skyddat privatliv. På nätet, och i köttvärlden.</li>
                    </ul>
                </p>
                <p>
                    Vi är en del av en internationell politisk rörelse. Vi reser på äventyr tillsammans, träffar pirater från hela världen, pratar med folk på stan om piratpolitik och har pizzahäng. Bli medlem på <a href="https://blipirat.nu">blipirat.nu</a> och få mailuppdateringar om vad som händer!
                </p>
                <p>
                    Som medlem kan du även chatta med oss och andra pirater på <a href="https://chat.piratpartiet.se">chat.piratpartiet.se</a>. Släng iväg ett mail till <a href="mailto:info@ungpirat.se">info@ungpirat.se</a> så släpper vi in dig i chatten!
                    Vill du veta mer om Piratpolitik?
                </p>
                <p>
                    Du kan gå in på <a href="https://piratpartiet.se">piratpartiet.se</a> för att läsa mer om piratpolitik i Sverige.
                    För allmäna frågor kontakta oss på <a href="mailto:info@ungpirat.se">info@ungpirat.se</a>. Du kan även kontakta oss krypterat via appen signal på 076 307 84 41
                </p>
            </div>
        </div>
    </Layout>
)

export default EventsPage
