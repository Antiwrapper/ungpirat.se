import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Logo, { Icon } from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
    <Layout>
        <SEO title="Hem" />
        <section className="hero is-link is-fullheight-with-navbar">
            <div className="hero-body">
                <div className="container">
                    <p className="title is-size-1 has-text-weight-light">
                        Vi tror på <strong className="has-text-weight-bold">framtiden</strong>,
                        <br />
                        gör du?
                    </p>
                    <a className="button is-primary" href="https://blipirat.nu">Bli medlem</a>
                </div>
            </div>
        </section>
    </Layout>
)

export default IndexPage
