import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Hero from "../components/hero"

const LinksPage = () => (
    <Layout>
        <SEO title="Länkar" />
        <Hero heroTitle="Länkar" />
        <div id="aboutContent" className="container">
            <div className="content">
                <div className="tile is-ancestor">
                    <div className="tile is-parent">
                        <div className="tile is-child has-text-centered link-notification">
                            <p class="title"><i className="fas fa-edit"></i >Docs</p>
                        </div>
                    </div>
                    <div className="tile is-parent">
                        <div className="tile is-child has-text-centered link-notification">
                            <p class="title"><i className="fas fa-archive"></i> Dokumentarkiv</p>
                        </div>
                    </div>
                    <div className="tile is-parent">
                        <div className="tile is-child has-text-centered link-notification">
                            <p class="title"><i className="fab fa-google-drive"></i> G Suite</p>
                        </div>
                    </div>
                </div>
                <div className="tile is-ancestor">
                    <div className="tile is-parent">
                        <div className="tile is-child has-text-centered link-notification">
                            <p class="title has-"><i className="fas fa-bus-alt"></i> Resebokning</p>
                        </div>
                    </div>
                    <div className="tile is-parent">
                        <div className="tile is-child has-text-centered link-notification">
                            <p class="title"><i class="fas fa-cloud-upload-alt"></i> Piratecloud</p>
                        </div>
                    </div>
                    <div className="tile is-parent">
                        <div className="tile is-child has-text-centered link-notification">
                            <p class="title"><i class="fas fa-store"></i> Pirateshop</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </Layout>
)

export default LinksPage
