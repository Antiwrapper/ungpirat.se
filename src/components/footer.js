import PropTypes from "prop-types"
import React from "react"
import Logo from "../images/logo-ung-pirat-white.png"
import CCIcon from "../images/cc_white.svg"
import AttributionIcon from "../images/attribution_icon_white.svg"

const Footer = ({ siteTitle, siteAuthor }) => (
    <footer class="footer">
        <div class="content" style={{paddingTop: "1rem"}}>
            <div class="columns">
                <div class="column is-half">
                    <img class="logo-footer" src={Logo} alt="Ung Pirats logotyp" />
                    <br/>
                    <p>
                    Ung Pirat har som avsikt att försöka publicera allt innehåll under licensen <a href="https://creativecommons.org/licenses/by/4.0/legalcode.sv">Creative Commons</a>, men kan kanske inte göra det med allt material. För att använda­/återanvända material från sidan, ansvarar du själv för att se till att du har rätt tillstånd att göra så.
                    </p>
                    © {siteAuthor} {new Date().getFullYear()}, byggt med {' '} <a href="https://www.gatsbyjs.org">Gatsby</a> och <a href="https://bulma.io">Bulma</a>.
                    <br/>
                    <br/>
                    <a rel="license" href="https://creativecommons.org/licenses/by/4.0/" title="Creative Commons Attribution 4.0 International license">
                        <img class="cc-footer" src={CCIcon}/>
                        <img class="cc-footer" src={AttributionIcon}/>
                    </a>
                </div>
            </div>
        </div>
    </footer>
)

Footer.propTypes = {
    siteTitle: PropTypes.string,
}

Footer.defaultProps = {
    siteTitle: "Ung Pirat",
}

export default Footer
