import React from "react"

const Hero = ({ heroTitle }) => (
    <section className="hero is-link is-">
        <div className="hero-body">
            <div className="container">
                <p className="title is-size-1 has-text-weight-light has-text-centered">
                    {heroTitle}
                </p>
            </div>
        </div>
    </section>
)

export default Hero;
