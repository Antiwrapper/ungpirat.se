import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import Logo from "../images/logo-ung-pirat.svg"
import Helmet from 'react-helmet'


class NavBar extends React.Component {
    state = {
        isActive: false
    }

    handleClick = () => {
        this.setState({ isActive: !this.state.isActive })
    }

    render() {
        return <nav className="navbar is-fixed-top" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
                <Link className="navbar-item" to="/">
                    <img src={Logo} alt="Ung Pirats logotyp" />
                </Link>
                <a className={this.state.isActive ? 'navbar-burger is-active' : 'navbar-burger'} aria-label="menu" aria-expanded="false" onClick={this.handleClick}>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>
            <div id="navbarMain" className={this.state.isActive ? 'navbar-menu is-active' : 'navbar-menu'}>
                <div className="navbar-start">
                </div>
                <div className="navbar-end">
                    <div className="navbar-item">
                        <Link className="navbar-item" to="/politics">
                            <i class="far fa-lightbulb"></i>&nbsp;
                            Vad tycker vi?
                        </Link>
                        <Link className="navbar-item" to="/news">
                            <i class="far fa-newspaper"></i>&nbsp;
                            Nyheter
                        </Link>
                        <Link className="navbar-item" to="/events">
                            <i class="far fa-calendar-alt"></i>&nbsp;
                            Evenemang
                        </Link>
                        <Link className="navbar-item" to="/activate">
                            <i class="fas fa-hiking"></i>&nbsp;
                            Bli aktiv
                        </Link>
                        <div className="navbar-item has-dropdown is-hoverable">
                            <div className="navbar-link">
                                Om oss
                                </div>
                            <div className="navbar-dropdown">
                                <Link className="navbar-item" to="/about">
                                    <i class="fas fa-history"></i>&nbsp;
                                    Om förbundet
                                </Link>
                                <Link className="navbar-item" to="/board">
                                    <i class="fas fa-users"></i>&nbsp;
                                    Förbundsstyrelsen
                                </Link>
                                <Link className="navbar-item" to="/hubs">
                                    <i class="fas fa-sitemap"></i>&nbsp;
                                    Hubbarna
                                </Link>
                                <Link className="navbar-item" to="/press">
                                    <i class="fas fa-address-book"></i>&nbsp;
                                    Kontakt och Press
                                </Link>
                                <hr className="navbar-divider" />
                                <Link className="navbar-item" href="https://admin.google.com/ungpirat.org" to="/links">
                                    <i class="fas fa-link"></i>&nbsp;
                                    Länkar
                                </Link>
                                <a className="navbar-item" href="http://wiki.ungpirat.se">
                                    <i class="fas fa-book-open"></i>&nbsp;
                                    Wiki
                                </a>
                            </div>
                        </div>
                        <div className="buttons">
                            <a className="button is-primary" href="https://blipirat.nu">
                                <strong>Bli medlem</strong>
                            </a>
                            <a className="button is-primary" href="https://pirateweb.net">
                                <strong>Logga in</strong>
                            </a>
                            <a className="button is-primary" href="https://chat.piratpartiet.se">
                                <strong>Chatt</strong>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    }
}

const Header = ({ siteTitle }) => (
    <header>
        <Helmet
            bodyAttributes={{
                class: 'has-navbar-fixed-top'
            }}
        />
        <NavBar>
        </NavBar>
    </header>
)

Header.propTypes = {
    siteTitle: PropTypes.string,
}

Header.defaultProps = {
    siteTitle: `Ung Pirat`,
}

export default Header
