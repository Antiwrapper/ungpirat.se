import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import logoSVG from "../images/logo-ung-pirat.svg"
import iconSVG from "../images/icon-ung-pirat.svg"

/*
 * This component is built using `gatsby-image` to automatically serve optimized
 * images with lazy loading and reduced file sizes. The image is loaded using a
 * `useStaticQuery`, which allows us to load the image from directly within this
 * component, rather than having to pass the image data down from pages.
 *
 * For more information, see the docs:
 * - `gatsby-image`: https://gatsby.dev/gatsby-image
 * - `useStaticQuery`: https://www.gatsbyjs.org/docs/use-static-query/
 */

const Logo = () => {
    const data = useStaticQuery(graphql`
        query {
            placeholderImage: file(relativePath: { eq: "logo-ung-pirat.png" }) {
                childImageSharp {
                    fluid(maxWidth: 300) {
                        ...GatsbyImageSharpFluid
                    }
                }
            }
        }
    `)

    return <img src={data.placeholderImage.childImageSharp.fluid} alt="Ung Pirats logotyp" />
}

const LogoWhite = () => {
    const data = useStaticQuery(graphql`
        query {
            placeholderImage: file(relativePath: { eq: "logo-ung-pirat-white.png" }) {
                childImageSharp {
                    fluid(maxWidth: 2000) {
                        ...GatsbyImageSharpFluid
                    }
                }
            }
        }
    `)

    return <Img fluid={data.placeholderImage.childImageSharp.fluid} alt="Ung Pirats logotyp" />
}

const LogoSVG = (props) => {
    return <img style={props} src={logoSVG} alt="Ung Pirats logotyp" />
}

const Icon = (props) => {
    return <img style={props} src={iconSVG} alt="Orange piratflagga" />
}

export default Logo;

export {
    LogoWhite,
    Icon,
    LogoSVG,
}