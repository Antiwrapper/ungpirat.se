# Ung Pirats nya sida
Ett demo ihop satt av mig som visar en ny hemsida för [Ung Pirat](ungpirat.se).

Sidan är byggd med:
- [GatsbyJS](https://www.gatsbyjs.org/)
- [Bulma](https://bulma.io/)

Tanken är att använda Gatsby för att hämta data ifrån en Wordpress instans och som sedan genererar en statisk sida som är self-hosted.