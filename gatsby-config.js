module.exports = {
  siteMetadata: {
    title: "Ung Pirat",
    description: "Ung Pirat | Piratpartiets ungdomsförbund.",
    author: "Ung Pirat",
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: "gatsby-source-wordpress",
      options: {
        baseUrl: "https://ungpirat.se",
        protocol: "https",
        hostingWPCOM: false,
        useACF: false,
        auth: {
          wpcom_app_clientId: process.env.WORDPRESS_CLIENT_ID,
          wpcom_app_clientSecret: process.env.WORDPRESS_CLIENT_SECRET,
          wpcom_user: process.env.WORDPRESS_USER,
          wpcom_pass: process.env.WORDPRESS_PASSWORD
        },
        searchAndReplaceContentUrls: {
          sourceUrl: "https://ungpirat.se",
          replacementUrl: "https://ungpirat.se"
        }
      }
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-purgecss`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `ung-pirat`,
        short_name: `ung-pirat`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/icon-ung-pirat.svg`,
      },
    },
  ],
}
